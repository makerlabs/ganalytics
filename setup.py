from setuptools import setup
from os import path
from pip.req import parse_requirements

requirements_location = path.join(path.dirname(__file__), "requirements.txt")
install_reqs = parse_requirements(requirements_location)
reqs = [str(ir.req) for ir in install_reqs]

setup(
    name='ganalytical',
    version='0.0.1',
    py_modules=['ganalytics'],
    install_requires=reqs,
)
