import datetime
from hamcrest import assert_that, has_length, equal_to, is_not, has_key
import httpretty
from os import path
import requests
import unittest

from ganalytical import Ganalytical, GanalyticalException

def load_fixture_string(fixture_name):
    fixture_path = path.join(path.dirname(__file__), "test_responses", fixture_name)
    with open(fixture_path) as infile:
        return infile.read()

class GanalyticalTestCase(unittest.TestCase):

    def setUp(self):
        httpretty.enable()
        self.addCleanup(httpretty.disable)
        self.session = requests.Session()

class ErrorResponseTestCase(GanalyticalTestCase):

    def test_throws_on_error(self):
        httpretty.register_uri(
            httpretty.GET,
            "https://www.googleapis.com/analytics/v3/data/ga",
            body=load_fixture_string("error_response.json")
        )
        client = Ganalytical(self.session)
        with self.assertRaises(GanalyticalException) as ctx:
            client.get(
                "someid",
                ["ga:browser"],
                ["dimension1"],
                datetime.datetime(2014, 1, 1, 10),
                datetime.datetime(2014, 1, 2, 11),
            )
        assert_that(ctx.exception.error_json, has_key("message"))
        assert_that(ctx.exception.metrics, equal_to(["ga:browser"]))
        assert_that(ctx.exception.dimensions, equal_to(["dimension1"]))



class SuccessfulDataTestCase(GanalyticalTestCase):

    def make_sucessful_request(self, dimensions=["ga:browser"], metrics=["ga:sessions"]):
        httpretty.register_uri(
            httpretty.GET,
            "https://www.googleapis.com/analytics/v3/data/ga",
            body=load_fixture_string("successfull_response.json")
        )

        client = Ganalytical(self.session)
        result = client.get(
            "someid",
            metrics,
            dimensions,
            datetime.datetime(2014, 1, 1, 10),
            datetime.datetime(2014, 1, 2, 11),
        )
        return result

    def test_returns_rows_of_response(self):
        result = self.make_sucessful_request()
        assert_that(result, has_length(30))
        assert_that(result[0], [
            "Amazon Silk",
            "89",
        ])

    def test_requests_without_dimensions_if_not_set(self):
        result = self.make_sucessful_request(
            dimensions=None,
        )
        request = httpretty.last_request()
        assert_that(request.querystring, is_not(has_key("dimensions")))

    def test_formats_dates_correctly(self):
        result = self.make_sucessful_request()
        request = httpretty.last_request()
        start_date = request.querystring["start-date"][0]
        assert_that(start_date, equal_to("2014-01-01"))
        end_date = request.querystring["end-date"][0]
        assert_that(end_date, equal_to("2014-01-02"))

    def test_prepends_ga_to_profile_id(self):
        result = self.make_sucessful_request()
        request = httpretty.last_request()
        assert_that(request.querystring["ids"][0], equal_to("ga:someid"))


class SuccessFulGetProfilesTestCase(GanalyticalTestCase):

    def make_successufull_request(self):
        httpretty.register_uri(
            httpretty.GET,
            "https://www.googleapis.com/analytics/v3/management/accounts",
            body=load_fixture_string("successfull_accounts_response.json")
        )

        client = Ganalytical(self.session)
        result = client.get_accounts()
        return result

    def test_returns_accounts_from_response(self):
        result = self.make_successufull_request()
        assert_that(result, has_length(2))
        assert_that(result[0]["id"], "20747234")


class SuccessfulViewsTestCase(GanalyticalTestCase):

    def make_successful_request(self):
        httpretty.register_uri(
            httpretty.GET,
            "https://www.googleapis.com/analytics/v3/management/accounts/~all/webproperties/~all/profiles",
            body=load_fixture_string("successfull_views_response.json")
        )

        client = Ganalytical(self.session)
        result = client.get_views()
        return result

    def test_returns_items(self):
        result = self.make_successful_request()
        assert_that(result, has_length(2))
        assert_that(result[0]["name"], "some view name")
