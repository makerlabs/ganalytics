from requests_oauthlib import OAuth2Session

class GanalyticalException(Exception):

    def __init__(self, error_json, metrics, dimensions):
        self.error_json = error_json
        self.metrics = metrics
        self.dimensions = dimensions
        self.message = "There was an error calling google analytics API, "\
            "error description was {0}\n arguments were metrics: {1} "\
            "dimensions were {2}".format(self.error_json,
                                         self.metrics,
                                         self.dimensions)

class Ganalytical(object):

    def __init__(self, oauth_session):
        self.endpoint = "https://www.googleapis.com/analytics/v3"
        self.oauth_session = oauth_session

    def get(self, profile_id, metrics, dimensions, start_date, end_date):
        params = {
            "ids": "ga:" + profile_id,
            "metrics": ",".join(metrics),
            "start-date": self._format_date(start_date),
            "end-date": self._format_date(end_date),
        }
        if dimensions is not None:
            params["dimensions"] = ",".join(dimensions)
        resp = self.oauth_session.get(self.endpoint + "/data/ga", params=params).json()
        if "error" in resp:
            raise GanalyticalException(resp["error"], metrics, dimensions)
        return resp["rows"]

    def get_accounts(self):
        resp = self.oauth_session.get(self.endpoint + "/management/accounts").json()
        return resp["items"]

    def get_views(self):
        resp = self.oauth_session.get(
            self.endpoint + "/management/accounts/~all/webproperties/~all/profiles"
        ).json()
        return resp["items"]

    def _format_date(self, date):
        return date.strftime("%Y-%m-%d")

