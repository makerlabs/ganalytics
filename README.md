#Ganalytics

A really thin wrapper around the google analytics API.

##Example Usage

    session = get_authorized_ga_session() // an authorized requests_oauthlib session for google analytics
    ga = GAnalytics(session)
